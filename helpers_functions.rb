# Helpers functions

# Returns bignumber into the binary representation.
def to_binary(num, padding=0, max_length=nil)
  bin = [num.to_s(16).rjust(padding*2, "0")].pack("H*")
  bin[0, max_length||bin.length]
end

def to_number(bin)
  bin.unpack("H*").first.to_i 16
end

